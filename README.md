magento
=======

Linklay plugin for Magento  
Version 1.0.0

**Instructions**

Insert a Linklay shoppable image using a variable shortcode with the following pattern:
{{block type="shortcodeembed/shortcodeembed" name="shortcodeembed_shortcodeembed" image_hash="[image]" template="shortcodeembed/shortcodeembed.phtml"}}
Replace [image] in the above pattern with the image code. Example: linklay5a0e064a5acef5.23861345

You can use the following parameters to further adjust the display characteristics of the image:
Horizontal alignment: align="right|center|right" (default: center)
CSS class: class="your_css_class_name"

For more information about Linklay shoppable images, or any question regarding the usage of this extension, visit https://Linklay.com.
