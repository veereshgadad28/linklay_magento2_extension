<?php
namespace Linklay\Shortcodeembed\Block;
 
class Shortcodeembed extends \Magento\Framework\View\Element\Template
{
    public function getHelloWorldTxt()
    {
        return 'Hello world!';
    }

    protected function _getLinklayHashCodeCollection()
    {
        $imageHashCode=$this->getImageHash();
        if ($imageHashCode) 
        {
            return  $imageHashCode;
        }
        else 
        {
            return null;
        }
    }

    protected function _getLinklayClassCollection()
    {
        $linklay_class_attributes=$this->getClassName();
        return $linklay_class_attributes;
    }

    protected function _getLinklayAlignmentCollection()
    {
        $atts_align=$this->getAlign();
        $text_align = null;
        if (!empty($atts_align)) 
        {
            switch (strtolower($atts_align)) 
            {
                case 'center':
                    $text_align = 'text-align:center;';
                    break;
                case 'right':
                    $text_align = 'text-align:right;';
                    break;
                case 'left':
                    $text_align = 'text-align:left;';
                    break;
            } 
            return $text_align;
        } 
        else {
            return 'text-align:center;';
        }
    }
    
    public function getLoadedLinklayHashCodeCollection()
    {
        return $this->_getLinklayHashCodeCollection();
    }
    
    public function getLoadedLinklayAlignment()
    {
        return $this->_getLinklayAlignmentCollection();
    }
    
    public function getLoadedLinklayClass()
    {
        return $this->_getLinklayClassCollection();
    }
    
}
